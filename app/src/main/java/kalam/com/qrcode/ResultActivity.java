package kalam.com.qrcode;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class ResultActivity extends AppCompatActivity {

    ImageView imageResult;
    TextView textResult;
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        imageResult = findViewById(R.id.imageResult);
        textResult = findViewById(R.id.textResult);
        relativeLayout = findViewById(R.id.resultLayout);

        Typeface rajdhani = Typeface.createFromAsset(getAssets(), "fonts/rajdhani-semibold.ttf");
        textResult.setTypeface(rajdhani);

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        getBundle();
    }

    private void getBundle() {
        Bundle extras = getIntent().getExtras();
        String message, from;
        Integer statusCode;

        if (extras != null) {
            from = extras.getString("from");
            message = extras.getString("resultMessage");
            statusCode = extras.getInt("statusCode");

//            Toast.makeText(ResultActivity.this, "Response Is : " + message, Toast.LENGTH_LONG).show();
            Log.e("ResultMessage ", message );

//            if (from.equals("success")) {
//                textResult.setText(message);
//            } else if (from.equals("failed")) {
//                JSONObject object = null;
//                try {
//                    object = new JSONObject(message);
//                    textResult.setText(object.getString("error"));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }

//            Toast.makeText(ResultActivity.this, "Response Is : "+ message, Toast.LENGTH_LONG).show();
//            Toast.makeText(ResultActivity.this, "Response Is : "+ statusCode, Toast.LENGTH_LONG).show();

            switch (statusCode) {
                case 202:
                    imageResult.setImageResource(R.drawable.ic_check);
                    textResult.setTextColor(this.getResources().getColor(R.color.blueIcon));
                    textResult.setText(message);
                    break;
                case 406:
                    imageResult.setImageResource(R.drawable.ic_cancel);
                    textResult.setTextColor(this.getResources().getColor(R.color.redIcon));
                    textResult.setText(message);
                    break;
                case 404:
                    imageResult.setImageResource(R.drawable.ic_help);
                    textResult.setTextColor(this.getResources().getColor(R.color.redIcon));
                    JSONObject object = null;
                    try {
                        object = new JSONObject(message);
                        textResult.setText(object.getString("error"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
