package kalam.com.qrcode;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;

//implementing onclicklistener
public class MainActivity extends AppCompatActivity {

    //View Objects
//    private Button buttonScan;
    private TextView textTitle, textDesc;
    private ImageView imageTitleA, imageTitleB;

    //qr code scanner object
    private IntentIntegrator qrScan;

    android.app.AlertDialog dialogBuilder;

    // Instantiate the RequestQueue.
    RequestQueue queue;
    String url ="http://128.199.197.161/api/v1/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dialogBuilder = new android.app.AlertDialog.Builder(MainActivity.this).create();
        queue = Volley.newRequestQueue(MainActivity.this);

        textTitle = findViewById(R.id.textTitle);
        textDesc = findViewById(R.id.textDesc);
        imageTitleA = findViewById(R.id.imageTitle);
        imageTitleB = findViewById(R.id.imageTitleB);

        Typeface roboto = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        textTitle.setTypeface(roboto);
        textDesc.setTypeface(roboto);

        imageTitleA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //initiate scan with our custom scan activity
                new IntentIntegrator(MainActivity.this).setCaptureActivity(ScannerActivity.class).initiateScan();
            }
        });
        imageTitleB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //initiate scan with our custom scan activity
                new IntentIntegrator(MainActivity.this).setCaptureActivity(ScannerActivity.class).initiateScan();
            }
        });

        //Scan Button
//        Button buttonBarCodeScan = findViewById(R.id.buttonScan);
//        buttonBarCodeScan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //initiate scan with our custom scan activity
//                new IntentIntegrator(MainActivity.this).setCaptureActivity(ScannerActivity.class).initiateScan();
//            }
//        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, Intent data) {
        //We will get scan results here
        final IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        //check for null
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Scan Cancelled", Toast.LENGTH_LONG).show();
            } else {
//                Toast.makeText(MainActivity.this, result.getContents(), Toast.LENGTH_LONG).show();
                Log.e("Contents ", result.getContents() );
                //show dialogue with result
//                showResultDialogue(result.getContents());
                // Request a string response from the provided URL.
                url = url+result.getContents()+"/verify?token=69695cc05268a62ecaaf7015e5a2a0b427be6656a3acd2e04db771e6524e8569";
                Log.e("URL ", url);
//                Toast.makeText(MainActivity.this, url, Toast.LENGTH_LONG).show();
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("Masuk ", "ke berhasil");
                                dialogBuilder.dismiss();
                                // Display the first 500 characters of the response string.
//                                Toast.makeText(MainActivity.this, "Response Is : "+ response, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                                intent.putExtra("from", "success");
                                intent.putExtra("statusCode", 202);
                                intent.putExtra("resultMessage", response);
                                startActivity(intent);
                                finish();
                                url = "";
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Masuk ", "ke gagal");
                        dialogBuilder.dismiss();
                        String body = null;
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            // exception
                        }

//                        Toast.makeText(MainActivity.this, "Error Is : "+ error.networkResponse.statusCode+" "+error.getMessage()+" "+error.networkResponse.data.toString(), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                        intent.putExtra("from", "failed");
                        intent.putExtra("statusCode", error.networkResponse.statusCode);
                        intent.putExtra("resultMessage", body);
                        startActivity(intent);
                        finish();
                        url = "";
                    }
                });

// Add the request to the RequestQueue.
                queue.add(stringRequest);
                progressLoad();
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void progressLoad() {
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.loading, null);

        TextView txtKeterangan = dialogView.findViewById(R.id.txt_keterangan);
        Typeface rajdhani = Typeface.createFromAsset(getAssets(), "fonts/rajdhani-semibold.ttf");
        txtKeterangan.setTypeface(rajdhani);

        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        dialogBuilder.show();
    }

    //method to construct dialogue with scan results
    public void showResultDialogue(final String result) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Scan Result")
                .setMessage("Scanned result is " + result)
                .setPositiveButton("Copy result", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("Scan Result", result);
                        clipboard.setPrimaryClip(clip);
                        Toast.makeText(MainActivity.this, "Result copied to clipboard", Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();
    }
}